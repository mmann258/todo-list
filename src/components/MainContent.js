import React from "react"

const styles = {
    font: 100,
    color:"#FF8C00",
}

const date = new Date()
const hours = date.getHours()

let timeOfDay
if(hours>6 && hours<12){
    timeOfDay="Morning"
    styles.backgroundColor="#FF2D00"
}
else if(hours>=12 && hours < 19){
    timeOfDay="afternoon"
    styles.backgroundColor="#8914A3"
}
else{
    timeOfDay="Night"
    styles.backgroundColor="#FF5D00"
}

function MainContent(){
    return(
        <main style={styles}> 
            Good {timeOfDay}
        </main>
    )
}
export default MainContent
import React from "react"
function Product(props){
    return(
        <div> 
            <h3>Name: {props.name}</h3>
            <p>Price: {props.price.toLocaleString("en-US",{style: "currency",currency:"USD"})}

            - Description: {props.description}</p>
        </div>
    )
}
export default Product
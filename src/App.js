import React from "react"

import Header from "./components/Header"
import MainContent from "./components/MainContent"
import Footer from "./components/Footer"
import ProductsData from "./components/vschoolProducs"
import Product from "./components/Product"
import vschoolProducs from "./components/vschoolProducs"
import proudctsData from "./components/vschoolProducs"

//for another time
/*
class App extends React.Component{
    constructor() {
        super()
        this.state = {
            count: 0 
        }
    }

    render(){
        return {
        
    }
}
*/
function App(){
    const productsComponnets =proudctsData.map(product => { 
        return (
        <Product 
        key ={product.id}
        price ={product.price}
        description ={product.description}
        name = {product.name}
        />)
    })

    return(
        
        <div>
            {productsComponnets}
        </div>
    )
}

export default App